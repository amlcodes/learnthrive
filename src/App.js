import "semantic-ui-css/semantic.min.css";
import "./App.css";
import React, { Component } from "react";
import Dropzone from "react-dropzone";
import ReactGoogleMapLoader from "react-google-maps-loader";
import ReactGooglePlacesSuggest from "react-google-places-suggest";
import {
  Button,
  Header,
  Image,
  Modal,
  Icon,
  Form,
  Input,
  Popup,
  Label,
  Checkbox
} from "semantic-ui-react";
import axios from "axios";
import qs from "qs";
import { Container, Row, Col, UncontrolledCarousel } from "reactstrap";
import * as Scroll from "react-scroll";
import {
  Link,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller
} from "react-scroll";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import TwitterLogin from "react-twitter-auth/lib/react-twitter-auth-component.js";
import GoogleLogin from "react-google-login";
import pp from "point-in-polygon";
import Avatar from "react-avatar-edit";
import fs from "fs";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth,
      // modalOpen: true,
      // termsOpen: true,
      // employmentOpen: true,
      // uploadModalOpen: true,
      // termsOpen: true,
      // verificationOpen: true,
      // src:
      // "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2080200692295969&height=235&width=220&ext=1539896261&hash=AeREOPOp5UD1AOPD",
      email: "",
      preview: null,
      triedsubmit: false,
      loadedItems: [],
      loaded: false,
      customerfacing: false,
      igsrcset:
        "/assets/mainpage/instagram.png, /assets/mainpage/instagram@2x.png 2x",
      igsrc: "/assets/mainpage/instagram.png",
      twittersrcset:
        "/assets/mainpage/twitter.png, /assets/mainpage/twitter@2x.png 2x",
      twittersrc: "/assets/mainpage/twitter.png",
      fbsrcset:
        "/assets/mainpage/facebook.png, /assets/mainpage/facebook@2x.png 2x",
      fbsrc: "/assets/mainpage/facebook.png"
    };
    this.submitPhotoUpload = this.submitPhotoUpload.bind(this);
  }

  handleChange = (e, { value }) => {
    this.setState({ [e.target.id]: e.target.value });
  };
  handleCodeChange = e => {
    if (e.target.value.length <= 1) {
      this.setState({ verifycode: e.target.value });
    }
    if (e.target.value.length === 1) {
      // ReactDOM.findDOMNode(this.verifycode2).focus();
      document.getElementById("verifycode2").focus();
    }
  };
  handleCodeChange2 = e => {
    let code = this.state.verifycode;
    if (e.target.value.length <= 1) {
      this.setState({ verifycode: code + e.target.value });
    }
    if (e.target.value.length === 1) {
      // ReactDOM.findDOMNode(this.verifycode3).focus();
      document.getElementById("verifycode3").focus();
    }
  };
  handleCodeChange3 = e => {
    let code = this.state.verifycode;
    if (e.target.value.length <= 1) {
      this.setState({ verifycode: code + e.target.value });
    }
    if (e.target.value.length === 1) {
      document.getElementById("verifycode4").focus();
    }
  };
  handleCodeChange4 = e => {
    let code = this.state.verifycode;
    if (e.target.value.length <= 1) {
      this.setState({ verifycode: code + e.target.value });
    }
  };

  cursorFocus = elem => {
    var x = window.scrollX,
      y = window.scrollY;
    elem.focus();
    window.scrollTo(x, y);
  };
  scrollToTop() {
    scroll.scrollToTop();
  }
  scrollToBottom() {
    scroll.scrollToBottom();
  }
  scrollTo() {
    scroll.scrollTo(100);
  }
  scrollMore() {
    scroll.scrollMore(100);
  }
  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };
  goto = string => {
    return string;
  };
  onClose = () => {
    this.setState({ preview: null });
  };
  onCrop = preview => {
    this.setState({ preview });
  };
  handleOpen = () => this.setState({ modalOpen: true });
  handleClose = () => this.setState({ modalOpen: false });
  toggle = () => {
    this.setState(
      { customerfacing: !this.state.customerfacing },
      function() {}
    );
  };
  fblinkhover = () => {
    this.setState({
      fbsrcset:
        "/assets/mainpage/facebookhover.png, /assets/mainpage/facebookhover@2x.png 2x",
      fbsrc: "/assets/mainpage/facebookhover.png"
    });
  };
  fblink = () => {
    this.setState({
      fbsrcset:
        "/assets/mainpage/facebook.png, /assets/mainpage/facebook@2x.png 2x",
      fbsrc: "/assets/mainpage/facebook.png"
    });
  };
  twitterlinkhover = () => {
    this.setState({
      twittersrcset:
        "/assets/mainpage/twitterhover.png, /assets/mainpage/twitterhover@2x.png 2x",
      twittersrc: "/assets/mainpage/twitterhover.png"
    });
  };
  twitterlink = () => {
    this.setState({
      twittersrcset:
        "/assets/mainpage/twitter.png, /assets/mainpage/twitter@2x.png 2x",
      twittersrc: "/assets/mainpage/twitter.png"
    });
  };
  iglinkhover = () => {
    this.setState({
      igsrcset:
        "/assets/mainpage/ighover.png, /assets/mainpage/instagramhover@2x.png 2x",
      igsrc: "/assets/mainpage/ighover.png"
    });
  };
  iglink = () => {
    this.setState({
      igsrcset: "/assets/mainpage/ig.png, /assets/mainpage/instagram@2x.png 2x",
      igsrc: "/assets/mainpage/ig.png"
    });
  };
  PrivacyPolicy = () => {
    this.setState({
      privacyModal: true
    });
  };
  contactModal = () => {
    this.setState({
      contactModal: true
    });
  };
  productTerm = () => {
    this.setState({
      productTermsModal: true
    });
  };
  exitModals = () => {
    this.setState({
      termsOpen: false,
      thanksOpen: false,
      outofMarket: false,
      uploadPhotoOpen: false,
      userExists: false,
      uploadModalOpen: false,
      employmentOpen: false,
      verificationOpen: false,
      modalOpen: false,
      verifycode: null,
      token: null,
      productTermsModal: false,
      contactModal: false,
      privacyModal: false
    });
  };

  componentWillMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);

    // const script = document.createElement("script");
    // script.src =
    //   "https://maps.googleapis.com/maps/api/js?key=" +
    //   API_KEY +
    //   "&libraries=places";
    // document.head.append(script);
    // this.setState({ loaded: true });
  }
  componentDidMount() {
    this.cursorFocus(document.getElementById("email"));
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handleEmployerChange = e => {
    this.setState({
      employersearch: e.target.value,
      employervalue: e.target.value
    });
  };
  handleLocationChange = e => {
    this.setState({
      locationsearch: e.target.value,
      locationvalue: e.target.value
    });
  };
  handleEmployerSelectSuggest = (suggest, originalPrediction) => {
    var count = (originalPrediction.description.match(/,/g) || []).length;
    console.log(suggest);

    if (count > 3) {
      let firstMatchedString = originalPrediction.description.split(",")[0];
      // console.log(suggest.geometry.location.lat());
      // console.log(suggest.geometry.location.lng());
      // let marketCondition = pp(
      //   [suggest.geometry.location.lng(), suggest.geometry.location.lat()],
      //   [
      //     [-74.036865234375, 40.694175391548754],
      //     [-74.05746459960938, 40.64938745451835],
      //     [-74.04098510742188, 40.57849862511043],
      //     [-73.99154663085938, 40.531545551348394],
      //     [-73.69354248046875, 40.57536944461837],
      //     [-73.7567138671875, 40.77742172100596],
      //     [-73.76083374023438, 40.83978809722463],
      //     [-73.71139526367188, 40.93426521177941],
      //     [-73.90777587890625, 40.95189982816191],
      //     [-73.92837524414061, 40.898981853950986],
      //     [-73.96270751953125, 40.83043687764923],
      //     [-74.00390625, 40.77742172100596],
      //     [-74.014892578125, 40.749337730454826],
      //     [-74.036865234375, 40.694175391548754]
      //   ]
      // );
      this.setState({
        employersearch: "",
        employervalue: firstMatchedString,
        locationvalue: suggest.formatted_address,
        lat: suggest.geometry.location.lat(),
        lng: suggest.geometry.location.lng()
        // outofMarket: marketCondition
      });
    } else {
      this.setState({
        employersearch: "",
        employervalue: originalPrediction.description
      });
    }
  };
  handleLocationSelectSuggest = (suggest, originalPrediction) => {
    console.log(suggest);
    console.log(originalPrediction);
    console.log(suggest.geometry.location.lat());
    console.log(suggest.geometry.location.lng());
    // let marketCondition = pp(
    //   [suggest.geometry.location.lng(), suggest.geometry.location.lat()],
    //   [
    //     [-74.036865234375, 40.694175391548754],
    //     [-74.05746459960938, 40.64938745451835],
    //     [-74.04098510742188, 40.57849862511043],
    //     [-73.99154663085938, 40.531545551348394],
    //     [-73.69354248046875, 40.57536944461837],
    //     [-73.7567138671875, 40.77742172100596],
    //     [-73.76083374023438, 40.83978809722463],
    //     [-73.71139526367188, 40.93426521177941],
    //     [-73.90777587890625, 40.95189982816191],
    //     [-73.92837524414061, 40.898981853950986],
    //     [-73.96270751953125, 40.83043687764923],
    //     [-74.00390625, 40.77742172100596],
    //     [-74.014892578125, 40.749337730454826],
    //     [-74.036865234375, 40.694175391548754]
    //   ]
    // );
    this.setState({
      locationsearch: "",
      locationvalue: originalPrediction.description,
      lat: suggest.geometry.location.lat(),
      lng: suggest.geometry.location.lng()
      // outofMarket: marketCondition
    });
  };

  openProfile = () => {
    this.setState({
      employmentSocialOpen: false,
      uploadModalOpen: true
    });
  };
  submitSignUp = () => {
    if (this.state.firstname && this.state.email && this.state.phone) {
      let self = this;
      if (this.state.lastname) {
        let data = {
          phone_number: this.state.phone,
          first_name: this.state.firstname,
          last_name: this.state.lastname,
          email: this.state.email,
          member_type: this.state.customerfacing ? "Employee" : "Customer"
        };
        const options = {
          method: "POST",
          headers: { "content-type": "application/x-www-form-urlencoded" },
          data: qs.stringify(data),

          // url: "http://localhost:3000/api/user/request_web_account"
          url:
            process.env.REACT_APP_HEROKU_STAGING +
            "/api/user/request_web_account"
        };
        axios(options)
          .then(function(response) {
            if ((response.data.success = "kinda")) {
              self.setState({
                modalOpen: false,
                // userExists: true,
                verificationOpen: true,

                triedsubmit: false,
                token: response.data.token
              });
            } else {
              self.setState({
                modalOpen: false,
                verificationOpen: true,
                triedsubmit: false,
                token: response.data.token
              });
            }
          })
          .catch(function(error) {
            console.log(error);
          });
      } else {
        let data = {
          phone_number: this.state.phone,
          first_name: this.state.firstname,
          email: this.state.email,
          member_type: this.state.customerfacing ? "Employee" : "Customer"
        };
        const options = {
          method: "POST",
          headers: { "content-type": "application/x-www-form-urlencoded" },
          data: qs.stringify(data),

          // url: "http://localhost:3000/api/user/request_web_account"
          url:
            process.env.REACT_APP_HEROKU_STAGING +
            "/api/user/request_phone_verification"
        };
        axios(options)
          .then(function(response) {
            self.setState({
              modalOpen: false,
              verificationOpen: true,
              triedsubmit: false,
              token: response.data.token
            });
          })
          .catch(function(error) {
            console.log(error);
          });
      }
    } else {
      this.setState({
        triedsubmit: true
      });
    }
  };
  getanotherVerification = () => {
    let self = this;
    console.log(this.state.verifycode);
    const data = {
      phone_number: this.state.phone
    };
    const options = {
      // url: "http://localhost:3000/api/user/request_web_account"
      method: "POST",
      headers: { "content-type": "application/x-www-form-urlencoded" },
      data: qs.stringify(data),

      url:
        process.env.REACT_APP_HEROKU_STAGING + "/api/user/request_web_account"
    };

    axios(options)
      .then(function(response) {
        self.setState({
          modalOpen: false,
          verificationOpen: true,
          triedsubmit: false,
          token: response.data.token
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  };
  submitVerification = () => {
    let self = this;
    console.log(this.state.verifycode);

    if (this.state.customerfacing && this.state.verifycode) {
      const data = {
        phone_number: this.state.phone,
        twilio_token: this.state.verifycode
      };
      const options = {
        method: "PATCH",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          "x-access-token": this.state.token
        },
        data: qs.stringify(data),

        // url: "http://localhost:3000/api/user/verify_phone"
        url: process.env.REACT_APP_HEROKU_STAGING + "/api/user/verify_phone"
      };
      console.log(data);
      console.log(options);
      axios(options)
        .then(function(response) {
          console.log(response);

          if (response.data == "updated User with verified phone number") {
            return self.setState({
              verificationOpen: false,
              triedsubmit: false,
              employmentOpen: true
            });
          }
        })
        .catch(function(error) {
          console.log(error);
        });
    } else if (this.state.verifycode) {
      const data = {
        phone_number: this.state.phone,
        twilio_token: this.state.verifycode
      };
      const options = {
        method: "PATCH",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          "x-access-token": this.state.token
        },
        data: qs.stringify(data),

        // url: "http://localhost:3000/api/user/verify_phone"
        url: process.env.REACT_APP_HEROKU_STAGING + "/api/user/verify_phone"
      };
      axios(options)
        .then(function(response) {
          console.log(response);
          if (response.data == "updated User with verified phone number") {
            return self.setState({
              triedsubmit: false,
              verificationOpen: false,
              uploadModalOpen: true
            });
          }
        })
        .catch(function(error) {
          console.log(error);
        });
    } else {
      self.setState({
        triedsubmit: true
      });
    }
  };
  submitEmployment = () => {
    if (
      !pp(
        [this.state.lng, this.state.lat],
        [
          [-74.036865234375, 40.694175391548754],
          [-74.05746459960938, 40.64938745451835],
          [-74.04098510742188, 40.57849862511043],
          [-73.99154663085938, 40.531545551348394],
          [-73.69354248046875, 40.57536944461837],
          [-73.7567138671875, 40.77742172100596],
          [-73.76083374023438, 40.83978809722463],
          [-73.71139526367188, 40.93426521177941],
          [-73.90777587890625, 40.95189982816191],
          [-73.92837524414061, 40.898981853950986],
          [-73.96270751953125, 40.83043687764923],
          [-74.00390625, 40.77742172100596],
          [-74.014892578125, 40.749337730454826],
          [-74.036865234375, 40.694175391548754]
        ]
      )
      // !d3Geo.geoContains(
      // {
      //   type: "FeatureCollection",
      //   features: [
      //     {
      //       type: "Feature",
      //       properties: {},
      //       geometry: {
      //         type: "Polygon",
      //         coordinates: [
      //           [
      //             [-74.036865234375, 40.694175391548754],
      //             [-74.05746459960938, 40.64938745451835],
      //             [-74.04098510742188, 40.57849862511043],
      //             [-73.99154663085938, 40.531545551348394],
      //             [-73.69354248046875, 40.57536944461837],
      //             [-73.7567138671875, 40.77742172100596],
      //             [-73.76083374023438, 40.83978809722463],
      //             [-73.71139526367188, 40.93426521177941],
      //             [-73.90777587890625, 40.95189982816191],
      //             [-73.92837524414061, 40.898981853950986],
      //             [-73.96270751953125, 40.83043687764923],
      //             [-74.00390625, 40.77742172100596],
      //             [-74.014892578125, 40.749337730454826],
      //             [-74.036865234375, 40.694175391548754]
      //           ]
      //         ]
      //       }
      //     }
      //   ]
      // },
      //
      // [this.state.lat, this.state.lat])
    ) {
      this.setState({
        employmentOpen: false,
        outofMarket: true
      });
    } else {
      let self = this;

      const data = {
        role: this.state.role,
        employer: this.state.employer,
        location: this.state.locationvalue,
        employee_id: this.state.employeeid
      };

      const options = {
        method: "PATCH",
        headers: {
          "content-type": "application/x-www-form-urlencoded",
          "x-access-token": this.state.token
        },
        data: qs.stringify(data),
        url: process.env.REACT_APP_HEROKU_STAGING + "/api/user"
      };
      axios(options)
        .then(function(response) {
          return self.setState({
            triedsubmit: false,
            employmentOpen: false,
            uploadModalOpen: true
          });
        })
        .catch(function(error) {
          console.log(error);
        });
    }
  };
  submitPhotoUpload = () => {
    // helper function: generate a new file from base64 String
    let self = this;
    //  const dataURLtoFile = (dataurl, filename) => {
    //   const arr = dataurl.split(",");
    //   const mime = arr[0].match(/:(.*?);/)[1];
    //   const bstr = atob(arr[1]);
    //   let n = bstr.length;
    //   const u8arr = new Uint8Array(n);
    //   while (n) {
    //     u8arr[n] = bstr.charCodeAt(n);
    //     n -= 1; // to make eslint happy
    //   }
    //   return new File([u8arr], filename, { type: mime });
    // };
    // let newimg = dataURLtoFile(this.state.preview, "img");
    const i = this.state.preview.indexOf("base64,");
    const buffer = Buffer.from(this.state.preview.slice(i + 7), "base64");
    const name = `${Math.random()
      .toString(36)
      .slice(-5)}.png`;
    const file = new File(buffer, name, { type: "image/png" });
    console.log("file", file);

    const formData = new FormData();
    console.log(this.state.preview);
    // console.log(newimg);

    if (this.state.preview) {
      formData.append("file", file);
      // formData.append("file", this.state.preview);

      const options = {
        method: "PATCH",
        headers: {
          "content-type": "multipart/form-data",
          "x-access-token": this.state.token
          // web: true
        }
      };

      // let url = "http://localhost:3000/api/user/photoupload";
      let url = process.env.REACT_APP_HEROKU_STAGING + "/api/user/photoupload";
      axios
        .patch(url, formData, options)
        .then(function(response) {
          return self.setState({
            uploadModalOpen: false,
            termsOpen: true
          });
          return response;
        })
        .catch(function(error) {
          console.log(error);
          return error;
        });
    } else {
      let self = this;

      return self.setState({
        uploadModalOpen: false,
        termsOpen: true
      });
    }
  };
  submitPhotoCrop = () => {
    this.setState({
      uploadPhotoOpen: false,
      termsOpen: true
    });
  };
  submitTerms = () => {
    this.setState({
      termsOpen: false,
      modalOpen: false,
      termsAccepted: true
      // thanksOpen: true
    });
  };

  responseGoogle = response => {
    console.log(response);
    this.setState({
      firstname: response.profileObj.name.split(" ")[0],
      lastname: response.profileObj.name.split(" ")[
        response.profileObj.name.split(" ").length - 1
      ],
      email: response.profileObj.email,
      profile_picture: response.profileObj.imageUrl
    });
    let self = this;
    let data = {
      first_name: response.profileObj.name.split(" ")[0],
      last_name: response.profileObj.name.split(" ")[
        response.profileObj.name.split(" ").length - 1
      ],
      email: response.email,
      profile_picture: response.profileObj.imageUrl,
      google_id: response.googleId,
      member_type: "Employee"
    };
    const options = {
      method: "POST",
      headers: { "content-type": "application/x-www-form-urlencoded" },
      data: qs.stringify(data),

      // url: "http://localhost:3000/api/user/request_web_account"
      url: process.env.REACT_APP_HEROKU_STAGING + "/api/user/googlelogin"
    };
    axios(options)
      .then(function(response) {
        self.setState({
          // modalOpen: false,
          // employmentSocialOpen: true,
          // triedsubmit: false,
          token: response.data.token
        });
      })
      .catch(function(error) {
        console.log(error);
      });

    // if (this.state.customerfacing) {
    //   this.setState({
    //     employmentOpen: true
    //   });
    // } else {
    //   this.setState({
    //     uploadPhoto: true
    //   });
    // }
  };
  onTwtterSuccess = response => {
    console.log(response);
  };
  failGoogle = response => {
    console.log(response);
    console.log(response.WE);
  };
  errorHandler = type => {
    console.log(type);
  };
  responseFacebook = response => {
    let self = this;
    console.log(response);
    this.setState({
      firstname: response.name.split(" ")[0],
      lastname: response.name.split(" ")[response.name.split(" ").length - 1],
      preview: response.picture.data.url,
      src: response.picture.data.url
    });
    let data = {
      first_name: response.name.split(" ")[0],
      last_name: response.name.split(" ")[response.name.split(" ").length - 1],
      fbid: response.userID,
      member_type: "Employee",
      // profile_picture: response.picture.data.url,
      facebook_token: response.accessToken
    };
    let fbtoken = response.accessToken;

    const options = {
      method: "POST",
      headers: { "content-type": "application/x-www-form-urlencoded" },
      data: qs.stringify(data),

      // url: "http://localhost:3000/api/user/request_web_account"
      url: process.env.REACT_APP_HEROKU_STAGING + "/api/user/fblogin"
    };
    axios(options)
      .then(function(response) {
        self.setState({
          // modalOpen: false,
          // employmentSocialOpen: true,
          // triedsubmit: false,
          token: response.data.token
          // fbt
        });
      })
      .catch(function(error) {
        console.log(error);
      });

    // if (this.state.customerfacing) {
    //   this.setState({
    //     employmentOpen: true
    //   });
    // } else {
    //   this.setState({
    //     uploadPhoto: true
    //   });
    // }
  };
  responseTwitter = response => {
    console.log(response);
    let self = this;
    let data = {
      first_name: response.name.split(" ")[0],
      last_name: response.name.split(" ")[response.name.split(" ").length - 1],
      twitterid: response.userID,
      member_type: "Employee"
    };
    console.log(data);
    const options = {
      method: "POST",
      headers: { "content-type": "application/x-www-form-urlencoded" },
      data: qs.stringify(data),

      // url: "http://localhost:3000/api/user/request_web_account"
      url: process.env.REACT_APP_HEROKU_STAGING + "/api/user/fblogin"
    };
    axios(options)
      .then(function(response) {
        self.setState({
          modalOpen: false,
          employmentSocialOpen: true,
          triedsubmit: false,
          token: response.data.token
        });
      })
      .catch(function(error) {
        console.log(error);
      });

    // if (this.state.customerfacing) {
    //   this.setState({
    //     employmentOpen: true
    //   });
    // } else {
    //   this.setState({
    //     uploadPhoto: true
    //   });
    // }
  };

  render() {
    const { width, errors } = this.state;
    const isMobile = width <= 500;
    const icon = (
      <svg viewBox="0 0 24 24" style={this.iconStyle}>
        <circle cx="12" cy="12" r="3.2" />
        <path d="M9 2L7.17 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2h-3.17L15 2H9zm3 15c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5z" />
      </svg>
    );
    const items = [
      {
        srcSet:
          "/assets/mainpage/baristamobile.png, /assets/mainpage/baristamobile@2x.png 2x",
        src: "/assets/mainpage/baristamobile.png"
      },
      {
        srcSet:
          "/assets/mainpage/deliverymobile.png, /assets/mainpage/deliverymobile@2x.png 2x",
        src: "/assets/mainpage/deliverymobile.png"
      },
      {
        srcSet:
          "/assets/mainpage/airlinemobile.png, /assets/mainpage/airlinemobile@2x.png 2x",
        src: "/assets/mainpage/airlinemobile.png"
      },
      {
        srcSet:
          "/assets/mainpage/cashiermobile.png, /assets/mainpage/cashiermobile@2x.png 2x",
        src: "/assets/mainpage/cashiermobile.png"
      },
      {
        srcSet:
          "/assets/mainpage/waitermobile.png, /assets/mainpage/waitermobile@2x.png 2x",
        src: "/assets/mainpage/waitermobile.png"
      },
      {
        srcSet:
          "/assets/mainpage/hardwaremobile.png, /assets/mainpage/hardwaremobile@2x.png 2x",
        src: "/assets/mainpage/hardwaremobile.png"
      }
    ];

    if (isMobile) {
      return (
        <Container className="App">
          <div className="section-1">
            <div className="applogo">
              <img
                srcSet="/assets/mainpage/hithrivelogo.png, /assets/mainpage/hithrivelogo@2x.png 2x"
                src="/assets/mainpage/hithrivelogo.png"
                className="responsivelogo"
                alt="logo"
              />
            </div>
            <div className="sectionContainer">
              <UncontrolledCarousel
                items={items}
                controls={false}
                indicators={false}
              />
              <div className="responsiveFlex">
                <p className="introtext">
                  Show appreciation to people who take time out of their day to
                  make yours. Join us if you’re interested in having a
                  meaningful impact on the future of positive interactions.
                </p>
                <Input
                  onChange={this.handleChange}
                  type="email"
                  className="email-box"
                  name="email"
                  id="email"
                />
                <Button
                  onClick={this.handleOpen}
                  type="button"
                  className="email-button"
                >
                  email me when it's ready
                </Button>
                <div
                  className={
                    "signupmodal " + (this.state.modalOpen ? "show-modal" : "")
                  }
                >
                  <div className="modal-content">
                    <img
                      srcSet="/assets/mainpage/emailbubble.png, /assets/mainpage/emailbubble@2x.png 2x"
                      src="/assets/mainpage/emailbubble.png"
                      className={
                        "emailbubble " +
                        (this.state.modalOpen ? "show-modal" : "")
                      }
                    />
                  </div>
                  <div className="modal-content">
                    <div className="nobordermodal-header">
                      <div className="flexcenterrow">
                        <h3 className="header-text">Sign up with </h3>
                      </div>
                      {/* <h3 className="header-text-option">Facebook</h3> */}
                      <div className="flexcenterrow">
                        <FacebookLogin
                          appId="688108541538759"
                          fields="name,email,picture.width(220).height(235)"
                          callback={response => {
                            this.responseFacebook(response);
                          }}
                          render={renderProps => (
                            <h3
                              className="header-text-option"
                              onClick={renderProps.onClick}
                            >
                              Facebook
                            </h3>
                          )}
                        />
                        <h3 className="header-text">, </h3>
                        {/* <h3 className="header-text-option">Twitter</h3> */}
                        {/* <div> */}
                        <TwitterLogin
                          loginUrl={
                            process.env.REACT_APP_HEROKU_STAGING +
                            "/api/user/twitter/auth"
                          }
                          onFailure={this.onFailed}
                          onSuccess={this.onTwtterSuccess}
                          requestTokenUrl={
                            process.env.REACT_APP_HEROKU_STAGING +
                            "/api/user/twitter/reverse"
                          }
                        >
                          {/* Twitter */}
                          <h3 className="header-text-option">Twitter</h3>
                        </TwitterLogin>
                        {/* </div> */}
                        <h3 className="header-text"> , or </h3>
                        <GoogleLogin
                          clientId="48507921891-tn1j0jhigq5dretfvavj00ulvd9rafqf.apps.googleusercontent.com"
                          onSuccess={response => this.responseGoogle(response)}
                          onFailure={fail => this.failGoogle(fail)}
                          render={renderProps => (
                            <h3
                              className="header-text-option"
                              onClick={renderProps.onClick}
                            >
                              Google
                            </h3>
                          )}
                        />
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="flexrow">
                      <img
                        srcSet="/assets/mainpage/orline.png, /assets/mainpage/orline@2x.png 2x"
                        src="/assets/mainpage/orline.png"
                        className="orline"
                      />
                      <h4 className="sidemargin">or</h4>
                      <img
                        srcSet="/assets/mainpage/orline.png, /assets/mainpage/orline@2x.png 2x"
                        src="/assets/mainpage/orline.png"
                        className="orline"
                      />
                    </div>
                    <Input
                      onChange={this.handleChange}
                      fluid
                      required={true}
                      id="firstname"
                      icon="user outline"
                      placeholder="First name"
                      value={this.state.firstname}
                      error={
                        !this.state.firstname && this.state.triedsubmit
                          ? true
                          : false
                      }
                    />
                    <Input
                      onChange={this.handleChange}
                      icon="user outline"
                      fluid
                      value={this.state.lastname}
                      id="lastname"
                      placeholder="Last name"
                    />
                    <Input
                      onChange={this.handleChange}
                      placeholder="joe@schmoe.com"
                      icon="envelope outline"
                      type="email"
                      id="email"
                      value={this.state.email}
                      error={
                        !this.state.email && this.state.triedsubmit
                          ? true
                          : false
                      }
                    />
                    <Input
                      onChange={this.handleChange}
                      id="phone"
                      icon="phone"
                      placeholder="10 - digit phone #"
                      error={
                        !this.state.phone && this.state.triedsubmit
                          ? true
                          : false
                      }
                    />
                    <div className="left">
                      <Checkbox
                        className="fontbox"
                        onChange={this.toggle}
                        checked={this.state.customerfacing}
                        id="customerfacing"
                        label="I WORK IN A CUSTOMER FACING ROLE"
                      />
                      <Popup
                        trigger={
                          <img
                            srcSet="/assets/mainpage/infoicon.png, /assets/mainpage/infoicon@2x.png 2x"
                            src="/assets/mainpage/infoicon.png"
                            onClick={this.infoicon}
                            className="infoicon"
                          />
                        }
                        content="Checking this box will prompt you to enter your employment information and set up your account as an associate. This gives other users the ability to share appreciation with you."
                      />
                    </div>
                    <Button
                      onClick={this.submitSignUp}
                      type="button"
                      className="signup-button"
                    >
                      Sign Up
                    </Button>
                  </div>
                </div>
                <div
                  className={
                    "userExistsModal " +
                    (this.state.userExists ? "show-userexistsmodal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          We already have your number!
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="flexleftcenter">
                      <p className="termsText">
                        Looks like you’ve already joined! Please contact us at
                        support@hithrive.com if we’ve made a mistake.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "verificationmodal " +
                    (this.state.verificationOpen
                      ? "show-verificationmodal"
                      : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-header">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">WE TEXTED YOU</h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="flexheightcentertext">
                      <p className="semimodaltext">
                        PLEASE VERIFY THE 4 - DIGIT NUMBER THAT WE SENT YOU.
                      </p>

                      <div className="verifycoderow">
                        <Input
                          id="verifycode1"
                          autoFocus
                          onChange={this.handleCodeChange}
                          error={this.state.triedsubmit ? true : false}
                          className="verifycode1"
                          ref={c => (this.inputref1 = c)}
                          maxLength="1"
                        />
                        <Input
                          id="verifycode2"
                          onChange={this.handleCodeChange2}
                          error={this.state.triedsubmit ? true : false}
                          className="verifycode2"
                          ref={c => (this.inputref2 = c)}
                          maxLength="1"
                        />
                        <Input
                          id="verifycode3"
                          onChange={this.handleCodeChange3.bind(this)}
                          error={this.state.triedsubmit ? true : false}
                          ref={this.inputref3}
                          className="verifycode3"
                          maxLength="1"
                        />
                        <Input
                          id="verifycode4"
                          onChange={this.handleCodeChange4.bind(this)}
                          error={this.state.triedsubmit ? true : false}
                          className="verifycode4"
                          ref={this.inputref4}
                          maxLength="1"
                        />
                      </div>
                      <a
                        onClick={this.getanotherVerification}
                        className="leftaction-text-option"
                      >
                        I DIDN'T RECEIVE THE CODE
                      </a>
                      <div className="verification-actions">
                        <Button
                          onClick={this.submitVerification}
                          type="button"
                          className="signup-button"
                        >
                          Confirm
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "employmentsocialmodal " +
                    (this.state.employmentSocialOpen
                      ? "show-employmentsocialmodal"
                      : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-header">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">EMPLOYMENT INFO</h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <Input
                      fluid
                      id="role"
                      onChange={this.handleChange}
                      placeholder="Role"
                      icon="user"
                      className="nomargin"
                    />

                    <ReactGoogleMapLoader
                      params={{
                        key: process.env.REACT_APP_GOOGLE_PLACES,
                        libraries: "places,geocode"
                      }}
                      render={googleMaps =>
                        googleMaps && (
                          <div>
                            <ReactGooglePlacesSuggest
                              autocompletionRequest={{
                                input: this.state.employersearch
                              }}
                              googleMaps={googleMaps}
                              textNoResults="New Employer" // null or "" if you want to disable the no results item
                              onSelectSuggest={this.handleEmployerSelectSuggest}
                            >
                              <input
                                type="text"
                                value={this.state.employervalue}
                                placeholder="Employer"
                                onChange={this.handleEmployerChange}
                              />
                            </ReactGooglePlacesSuggest>
                          </div>
                        )
                      }
                    />

                    <ReactGoogleMapLoader
                      params={{
                        key: process.env.REACT_APP_GOOGLE_PLACES,
                        libraries: "places,geocode"
                      }}
                      render={googleMaps =>
                        googleMaps && (
                          <div>
                            <ReactGooglePlacesSuggest
                              autocompletionRequest={{
                                input: this.state.locationsearch
                              }}
                              googleMaps={googleMaps}
                              textNoResults="New Location" // null or "" if you want to disable the no results item
                              onSelectSuggest={this.handleLocationSelectSuggest}
                            >
                              <input
                                type="text"
                                value={this.state.locationvalue}
                                placeholder="Search a location"
                                onChange={this.handleLocationChange}
                              />
                            </ReactGooglePlacesSuggest>
                          </div>
                        )
                      }
                    />

                    <Input
                      fluid
                      id="employeeid"
                      onChange={this.handleChange}
                      placeholder="Employee ID (if applicable)"
                      className="nomargin"
                    />
                    <p className="lefttext" onClick={this.openProfile}>
                      sign up as a customer
                    </p>
                    <div className="verification-actions">
                      <Button
                        onClick={this.submitEmployment}
                        type="button"
                        className="signup-button"
                      >
                        Confirm
                      </Button>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "outofMarketModal " +
                    (this.state.outofMarket ? "show-outofMarketModal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          We’re not in your area Yet :(
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">
                        For our first launch, we are focusing on the NYC-area
                        only. However, we have saved your information and will
                        notify you as soon as we are available in your area! You
                        may continue to use the app in the meantime. However,
                        people will not be able to search for you yet.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "employmentmodal " +
                    (this.state.employmentOpen ? "show-employmentmodal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-header">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">EMPLOYMENT INFO</h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <Input
                      fluid
                      id="role"
                      icon="user"
                      onChange={this.handleChange}
                      placeholder="Role"
                      className="nomargin"
                    />

                    <ReactGoogleMapLoader
                      params={{
                        key: process.env.REACT_APP_GOOGLE_PLACES,
                        libraries: "places,geocode"
                      }}
                      icon="user"
                      render={googleMaps =>
                        googleMaps && (
                          <div>
                            <ReactGooglePlacesSuggest
                              autocompletionRequest={{
                                input: this.state.employersearch
                              }}
                              googleMaps={googleMaps}
                              textNoResults="New Employer" // null or "" if you want to disable the no results item
                              onSelectSuggest={this.handleEmployerSelectSuggest}
                            >
                              <input
                                type="text"
                                value={this.state.employervalue}
                                placeholder="Employer"
                                onChange={this.handleEmployerChange}
                              />
                            </ReactGooglePlacesSuggest>
                          </div>
                        )
                      }
                    />
                    <ReactGoogleMapLoader
                      params={{
                        key: process.env.REACT_APP_GOOGLE_PLACES,
                        libraries: "places,geocode"
                      }}
                      render={googleMaps =>
                        googleMaps && (
                          <div>
                            <ReactGooglePlacesSuggest
                              autocompletionRequest={{
                                input: this.state.locationsearch
                              }}
                              googleMaps={googleMaps}
                              textNoResults="New Location" // null or "" if you want to disable the no results item
                              onSelectSuggest={this.handleLocationSelectSuggest}
                            >
                              <input
                                type="text"
                                value={this.state.locationvalue}
                                placeholder="Search a location"
                                onChange={this.handleLocationChange}
                              />
                            </ReactGooglePlacesSuggest>
                          </div>
                        )
                      }
                    />
                    <Input
                      fluid
                      id="employeeid"
                      onChange={this.handleChange}
                      placeholder="Employee ID (if applicable)"
                      className="nomargin"
                    />
                    <div className="verification-actions">
                      <Button
                        onClick={this.submitEmployment}
                        type="button"
                        className="signup-button"
                      >
                        Confirm
                      </Button>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "uploadmodal " +
                    (this.state.uploadModalOpen ? "show-uploadmodal" : "")
                  }
                >
                  <div className="modal-content">
                    <img
                      srcSet="/assets/mainpage/photoboomerbubble.png, /assets/mainpage/photoboomerbubble@2x.png 2x"
                      src="/assets/mainpage/photoboomerbubble.png"
                      className={
                        "photoboomerbubble " +
                        (this.state.modalOpen ? "show-modal" : "")
                      }
                    />
                  </div>
                  <div className="modal-content">
                    <div className="modal-header">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">UPLOAD PHOTO</h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="flexcentercolumntext">
                      <div>
                        <p className="semimodaltextcenter">
                          UPLOAD A PHOTO OF YOURSELF
                        </p>
                        <div className="fitbox">
                          <Avatar
                            width={220}
                            height={235}
                            onCrop={this.onCrop}
                            onClose={this.onClose}
                            src={this.state.src}
                            label="ADD PHOTO"
                            borderStyle={{
                              border: "4px solid #64CFDD",
                              textAlign: "center",
                              borderRadius: "50%",
                              mozBorderRadius: "50%",
                              webkitBorderRadius: "50%"
                            }}
                            labelStyle={{
                              color: "rgba(51, 51, 51, 0.7) !important",
                              fontSize: "14px !important",
                              width: "220px !important",
                              letterSpacing: "3px !important",
                              fontFamily: "Proxima Nova Semibold !important"
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="verification-actions">
                      <Button
                        onClick={this.submitPhotoUpload}
                        type="button"
                        className="signup-button"
                      >
                        Confirm
                      </Button>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "termsmodal " +
                    (this.state.termsOpen ? "show-termsmodal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          BEFORE YOU BEGIN
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">
                        Please agree to be respectful of other users (both
                        associates and customers).
                      </p>
                      <p className="termsText">
                        HiThrive is a platform built on the basis of showing
                        appreciation to customer-facing associates. If you have
                        complaints about an experience, this is not the place to
                        voice them.
                      </p>{" "}
                      <p className="termsText">
                        Joining HiThrive gives you the opportunity to help
                        associates receive the uplifting recognition that they
                        deserve. By agreeing to these terms, you are agreeing to
                        be a part of this community.
                      </p>
                    </div>

                    <div className="verification-actions">
                      <Button
                        onClick={this.submitTerms}
                        type="button"
                        className="signup-button"
                      >
                        I AGREE
                      </Button>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "privacyModal " +
                    (this.state.privacyModal ? "show-privacyModal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          HiThrive Privacy Policy
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">
                        Effective: August 10, 2018 This policy explains what
                        information we collect when you use HiThrive’s sites,
                        services, mobile applications, products, and content
                        (“Services”). It also has information about how we
                        store, use, transfer, and delete that information.
                        Information We Collect & How We Use It In order to give
                        you the best possible experience using HiThrive, we
                        collect information from your interactions with our
                        network. Some of this information, you actively tell us
                        (such as your email address and phone number, which we
                        use to track your account or communicate with you).
                        Other information, we collect based on actions you take
                        while using HiThrive, such as what content you access
                        and your interactions with our product features (like
                        comments, search, follows, and HiFives). This
                        information includes records of those interactions, your
                        Internet Protocol address, information about your device
                        (such as device or browser type), and referral
                        information. We use cookies and similar technologies
                        (like web beacons, pixels, ad tags and device
                        identifiers) to recognize you and/or your device(s) on,
                        off and across different Services and devices. You can
                        control cookies through your browser settings and other
                        tools. We use this information to: provide, test,
                        improve, promote and personalize HiThrive Services
                        generate relevant content and information including
                        advertising fight spam and other forms of abuse generate
                        aggregate, non-identifying information about how people
                        use HiThrive Services When you create your HiThrive
                        account, and authenticate with a third-party service
                        (like Twitter, Facebook or Google) we may collect,
                        store, and periodically update information associated
                        with that third-party account, such as your lists of
                        friends or followers. We will never publish through your
                        third-party account without your permission. Information
                        Disclosure Any data that you include on your profile and
                        any content you post or social action (like comments,
                        search, follows, and HiFives) you take on our Services
                        will be seen by others. We may share your account
                        information with third parties in some circumstances,
                        including: (1) with your consent; (2) to a service
                        provider or partner who meets our data protection
                        standards; (3) when we have a good faith belief it is
                        required by law, such as pursuant to a subpoena or other
                        legal process; (4) when we have a good faith belief that
                        doing so will help prevent imminent harm to someone. If
                        we are going to share your information in response to
                        legal process, we’ll give you notice so you can
                        challenge it (for example by seeking court
                        intervention), unless we’re prohibited by law or believe
                        doing so may endanger others. We will object to requests
                        for information about users of our services that we
                        believe are improper. Data Storage HiThrive uses
                        third-party vendors and hosting partners, such as
                        Amazon, for hardware, software, networking, storage, and
                        related technology we need to run HiThrive. We maintain
                        two types of logs: server logs and event logs. By using
                        HiThrive Services, you authorize HiThrive to transfer,
                        store, and use your information in the United States and
                        any other country where we operate. Modifying or
                        Deleting Your Personal Information If you have a
                        HiThrive account, you can access or modify your personal
                        information, or delete your account. HiThrive retains
                        personal data associated with your account for the
                        lifetime of your account. If you would like to delete
                        your personal information, you can delete your account
                        at any time. Upon deletion, your personal data will
                        generally stop being visible to others on our Services
                        within 24 hours. We generally delete closed account
                        information within 30 days of account closure, except as
                        noted below. We retain your personal data even after you
                        have closed your account when required to do so by law.
                        We will retain de-personalized information after your
                        account has been closed. Information you have shared
                        with others will remain visible after you closed your
                        account or deleted the information from your own profile
                        or inbox. Shared content associated with closed accounts
                        will show an unknown user as the source. Your profile
                        may continue to be displayed in the services of others
                        (e.g., search engine results) until they refresh their
                        cache. Data Security We use encryption (HTTPS/TLS) to
                        protect data transmitted to and from our site. However,
                        no data transmission over the Internet is 100% secure,
                        so we can’t guarantee security. You use the Service at
                        your own risk, and you’re responsible for taking
                        reasonable measures to secure your account. Business
                        Transfers If we are involved in a merger, acquisition,
                        bankruptcy, reorganization or sale of assets such that
                        your information would be transferred or become subject
                        to a different privacy policy, we’ll notify you in
                        advance so you can opt out of any such new policy by
                        deleting your account before transfer. Email from
                        HiThrive Sometimes we’ll send you emails about your
                        account, service changes or new policies. You can’t opt
                        out of this type of “transactional” email (unless you
                        delete your account). But, you can opt out of
                        non-administrative emails such as digests, newsletters,
                        and activity notifications through your account’s
                        “Settings” page. Changes to this Policy HiThrive may
                        periodically update this Policy. We’ll notify you about
                        significant changes to it. The most current version of
                        the policy will always be available at HiThrive.com and
                        under “Settings” within the mobile application.
                        Questions We welcome feedback about this policy at
                        legal@hithrive.com. Contact Information HiThrive
                        Incorporated 447 Broadway, 2nd Floor New York, NY 10013
                        United States of America
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "productTermsModal " +
                    (this.state.productTermsModal
                      ? "show-productTermsModal"
                      : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          HiThrive Terms of Service
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">
                        Effective: August 10, 2018 These Terms of Service
                        (“Terms”) are a contract between you and HIThrive
                        Incorporated (“HiThrive”). They govern your use of
                        HiThrive’s sites, services, mobile apps, products, and
                        content (“Services”). By using HiThrive, you agree to
                        these Terms. Your use of our Services is also subject to
                        our our Privacy Policy, which covers how we collect,
                        use, share, and store your personal information. If you
                        don’t agree to any of the Terms, you can’t use HiThrive.
                        We can change these Terms at any time. If a change is
                        material, we’ll let you know before they take effect. By
                        using HiThrive on or after that effective date, you
                        agree to the new Terms. If you don’t agree to them, you
                        should delete your account before they take effect,
                        otherwise your use of the Services will be subject to
                        the new Terms. Rights & Limits You own all of the
                        content and personal information you provide to us, but
                        by using HiThrive you also grant us a non-exclusive
                        license to it. The non-exclusive license is a worldwide,
                        transferable and sublicensable right to use, copy,
                        modify, distribute, publish, and process, information
                        and content that you provide through our Services,
                        without any further consent, notice and/or compensation
                        to you or others. You can end this license for specific
                        content by deleting such content from the Services, or
                        generally by closing your account, except (a) to the
                        extent you shared it with others as part of the Service
                        and they copied, re-shared it or stored it and (b) for
                        the reasonable time it takes to remove from backup and
                        other systems. We will not include your content in
                        advertisements for the products and services of third
                        parties to others without your separate consent
                        (including sponsored content). However, we have the
                        right, without payment to you or others, to serve ads
                        near your content and information, and your social
                        actions may be visible and included with ads, as noted
                        in the Privacy Policy. We will get your consent if we
                        want to give others the right to publish your content
                        beyond the Services. However, if you choose to share
                        your post as "public", we will enable a feature that
                        allows other Members to embed that public post onto
                        third-party services, and we enable search engines to
                        make that public content findable though their services.
                        While we may edit and make format changes to your
                        content (such as translating it, modifying the size,
                        layout or file type or removing metadata), we will not
                        modify the meaning of your expression. You and HiThrive
                        agree that if content includes personal data, it is
                        subject to our Privacy Policy. You and HiThrive agree
                        that we may access, store, process and use any
                        information and personal data that you provide in
                        accordance with the terms of the Privacy Policy and your
                        choices (including settings). By submitting suggestions
                        or other feedback regarding our Services to HiThrive,
                        you agree that HiThrive can use and share (but does not
                        have to) such feedback for any purpose without
                        compensation to you. You agree to only provide content
                        or information that does not violate the law nor
                        anyone’s rights (including intellectual property
                        rights). You also agree that your profile information
                        will be truthful. HiThrive may be required by law to
                        remove certain information or content in certain
                        countries. Our content and services We reserve all
                        rights in HiThrive’s look and feel. You may not copy or
                        adapt any portion of our code or visual design elements
                        (including logos) without express written permission
                        from HiThrive unless otherwise permitted by law. You may
                        not do, or try to do, the following: (1) access or
                        tamper with non-public areas of the Services, our
                        computer systems, or the systems of our technical
                        providers; (2) access or search the Services by any
                        means other than the currently available, published
                        interfaces (e.g., APIs) that we provide; (3) forge any
                        TCP/IP packet header or any part of the header
                        information in any email or posting, or in any way use
                        the Services to send altered, deceptive, or false
                        source-identifying information; or (4) interfere with,
                        or disrupt, the access of any user, host, or network,
                        including sending a virus, overloading, flooding,
                        spamming, mail-bombing the Services, or by scripting the
                        creation of content or accounts in such a manner as to
                        interfere with or create an undue burden on the
                        Services. Crawling the Services is allowed, but scraping
                        the Services is prohibited. We may change, terminate, or
                        restrict access to any aspect of the service, at any
                        time, without notice. No children HiThrive is only for
                        people 13 years old and over. By using HiThrive, you
                        affirm that you are over 13. If we learn someone under
                        13 is using HiThrive, we’ll terminate their account.
                        Security If you find a security vulnerability on
                        HiThrive, tell us. We are developing a bug bounty
                        disclosure program and will compensate you.
                        Vulnerabilities can be reported to
                        security@hithrive.com. Incorporated rules and policies
                        By using the Services, you agree to let HiThrive collect
                        and use information as detailed in our Privacy Policy.
                        If you’re outside the United States, you consent to
                        letting HiThrive transfer, store, and process your
                        information (including your personal information and
                        content) in and out of the United States. To enable a
                        functioning community, we have Rules and by using
                        HiThrive you agree to follow these Rules. If you don’t,
                        we may remove content, or suspend or delete your
                        account. Miscellaneous Disclaimer of warranty. HiThrive
                        provides the Services to you as is. You use them at your
                        own risk and discretion. That means they don’t come with
                        any warranty. None express, none implied. No implied
                        warranty of merchantability, fitness for a particular
                        purpose, availability, security, title or
                        non-infringement. Limitation of Liability. HiThrive
                        won’t be liable to you for any damages that arise from
                        your using the Services. This includes if the Services
                        are hacked or unavailable. This includes all types of
                        damages (indirect, incidental, consequential, special or
                        exemplary). And it includes all kinds of legal claims,
                        such as breach of contract, breach of warranty, tort, or
                        any other loss. No waiver. If HiThrive doesn’t exercise
                        a particular right under these Terms, that doesn’t waive
                        it. Severability. If any provision of these terms is
                        found invalid by a court of competent jurisdiction, you
                        agree that the court should try to give effect to the
                        parties’ intentions as reflected in the provision and
                        that other provisions of the Terms will remain in full
                        effect. Choice of law and jurisdiction. These Terms are
                        governed by New York law, without reference to its
                        conflict of laws provisions. You agree that any suit
                        arising from the Services must take place in a court
                        located in New York, New York. Entire agreement. These
                        Terms (including any document incorporated by reference
                        into them) are the whole agreement between HiThrive and
                        you concerning the Services. Questions? Let us know at
                        legal@hithrive.com.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "contactModal " +
                    (this.state.contactModal ? "show-contactModal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          We aren't there yet!
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">but we will be!</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Row className="section-2">
            <Col lg={6} md={6} sm={12} xs={12} className="section-text-a-1">
              <p className="header-number-a">1.</p>
              <p className="header-title">Find Someone</p>
              <img
                srcSet="/assets/mainpage/line.png, /assets/mainpage/line@2x.png 2x"
                src="/assets/mainpage/line.png"
                sizes="(max-width: 320px) 280px,
                                            (max-width: 480px) 440px,
                                            800px"
                className="line"
                alt="line"
              />
              <p className="body-right">
                The only social platform where you can attribute positive
                feedback directly to an employee.
              </p>
            </Col>
            <div className="phone2col">
              <picture>
                <img
                  srcSet="/assets/mainpage/phone2.png, /assets/mainpage/phone2@2x.png 2x"
                  src="/assets/mainpage/phone2.png"
                  className="phone2"
                  alt="search"
                />
              </picture>
            </div>
          </Row>
          <Row className="section-3">
            <Col className="section-text-b" lg={6} md={6} sm={12} xs={12}>
              <p className="header-number-b">2.</p>
              <p className="header-title-b">
                Say Something Positive & Share it
              </p>
              <img
                srcSet="/assets/mainpage/line.png, /assets/mainpage/line@2x.png 2x"
                src="/assets/mainpage/line.png"
                sizes="(max-width: 320px) 280px,
                                            (max-width: 480px) 440px,
                                            800px"
                className="line"
                alt="line"
              />
              <p className="body-left">
                A positive only platform, HiThrive subscribes to the adage “If
                you don’t have something nice to say, don’t say anything at
                all.”
              </p>
            </Col>
            <div className="phone3col">
              <img
                srcSet="/assets/mainpage/phone3.png, /assets/mainpage/phone3@2x.png 2x"
                src="/assets/mainpage/phone3.png"
                className="phone3"
                alt="positive"
              />
            </div>
          </Row>

          <Row className="section-4">
            <Col lg={6} md={6} sm={12} xs={12} className="section-text-a-2">
              <p className="header-number-a">3.</p>
              <p className="header-title">Thrive!</p>
              <img
                sizes="(max-width: 320px) 280px,
                                            (max-width: 480px) 440px,
                                            800px"
                srcSet="/assets/mainpage/line3.png, /assets/mainpage/line3@2x.png 2x"
                src="/assets/mainpage/line3.png"
                className="line"
                alt="line"
              />
              <p className="body-right-b">
                Collect gratitude to fuel your career. Whether you’re achieving
                milestones on the platform or being rewarded in your place of
                work, your employer will never again wonder what your value-add
                is. Allow us to cc your manager on each HiTHRIVE received and
                the process will work while you do.
              </p>
            </Col>

            <div className="phone4col">
              <img
                srcSet="/assets/mainpage/rocketship.png, /assets/mainpage/rocketship@2x.png 2x"
                src="/assets/mainpage/rocketship.png"
                className="phone4rocket"
                alt="Thrive"
              />
              <img
                srcSet="/assets/mainpage/phone4mobile.png, /assets/mainpage/phone4mobile@2x.png 2x"
                src="/assets/mainpage/phone4mobile.png"
                className="phone4"
                alt="Thrive"
              />
            </div>
          </Row>
          <Row className="section-footer">
            <div className="flex-row-footer">
              <div className="flexrowlinks">
                <li className="hiddenbullet">
                  <a href="https://facebook.com/Hithrive">
                    <img
                      srcSet="/assets/mainpage/facebook.png, /assets/mainpage/facebook@2x.png 2x"
                      src="/assets/mainpage/facebook.png"
                      onClick={this.goto("https://facebook.com/Hithrive")}
                      className="footer-header-img"
                    />
                  </a>
                </li>
                <li className="hiddenbullet">
                  <a href="https://instagram.com/hithrive">
                    <img
                      srcSet="/assets/mainpage/instagram.png, /assets/mainpage/instagram@2x.png 2x"
                      src="/assets/mainpage/instagram.png"
                      onClick={this.goto("https://instagram.com/hithrive")}
                      className="footer-header-img"
                    />
                  </a>
                </li>
                <li className="hiddenbullet">
                  <a href="https://twitter.com/hithrive">
                    <img
                      srcSet="/assets/mainpage/twitter.png, /assets/mainpage/twitter@2x.png 2x"
                      src="/assets/mainpage/twitter.png"
                      onClick={this.goto("https://twitter.com/hithrive")}
                      className="footer-header-img"
                    />
                  </a>
                </li>
              </div>
              <div className="flexrowlinks">
                <h3 className="footer-header-home">
                  <a href="#" className="footer-header-home">
                    HiThrive, 2018
                  </a>
                </h3>
                <h3
                  className="footer-header-option"
                  onClick={this.PrivacyPolicy}
                >
                  Privacy Policy
                </h3>
                <h3 className="footer-header-option" onClick={this.productTerm}>
                  Terms
                </h3>
                <h3
                  className="footer-header-option"
                  onClick={this.contactModal}
                >
                  Contact
                </h3>
              </div>
            </div>
          </Row>
        </Container>
      );
    } else {
      return (
        <Container className="App">
          <div className="section-1">
            <div className="applogo">
              <img
                srcSet="/assets/mainpage/hithrivelogo.png, /assets/mainpage/hithrivelogo@2x.png 2x"
                src="/assets/mainpage/hithrivelogo.png"
                className="responsivelogo"
                alt="logo"
              />
            </div>
            <div className="sectionContainer">
              <div className="picturesection">
                <div className="wrenchBox">
                  <div className="hardware-image">
                    <img
                      rel="preload"
                      srcSet="/assets/mainpage/hardware.png, /assets/mainpage/hardware@2x.png 2x"
                      src="/assets/mainpage/hardware.png"
                      className="responsive"
                      alt="intro"
                    />
                  </div>
                  <div className="hoverImg">
                    <img
                      rel="preload"
                      srcSet="/assets/mainpage/hardwareboomer.png, /assets/mainpage/hardwareboomer@2x.png 2x"
                      src="/assets/mainpage/hardwareboomer.png"
                      alt="intro"
                      className="biggerboomer"
                    />
                  </div>
                </div>
                <div className="airBox">
                  <div className="airline2-image">
                    <img
                      rel="preload"
                      className="responsive"
                      srcSet="/assets/mainpage/airline2.png, /assets/mainpage/airline2@2x.png 2x"
                      src="/assets/mainpage/airline2.png"
                      alt="intro"
                    />
                  </div>
                  <div className="hoverImg">
                    <img
                      rel="preload"
                      srcSet="/assets/mainpage/airlinerboomer.png, /assets/mainpage/airlinerboomer@2x.png 2x"
                      src="/assets/mainpage/airlinerboomer.png"
                      alt="intro"
                      className="boomer"
                    />
                  </div>
                </div>
                <div className="latteBox">
                  <div className="barista-image">
                    <img
                      rel="preload"
                      className="responsive"
                      srcSet="/assets/mainpage/barista.png, /assets/mainpage/barista@2x.png 2x"
                      src="/assets/mainpage/barista.png"
                      alt="intro"
                    />
                  </div>
                  <div className="hoverImg">
                    <img
                      rel="preload"
                      className="boomer"
                      srcSet="/assets/mainpage/baristaboomer.png, /assets/mainpage/baristaboomer@2x.png 2x"
                      src="/assets/mainpage/baristaboomer.png"
                      alt="intro"
                    />
                  </div>
                </div>
                <div className="deliveryBox">
                  <div className="delivery-image">
                    <img
                      rel="preload"
                      srcSet="/assets/mainpage/delivery.png, /assets/mainpage/delivery@2x.png 2x"
                      src="/assets/mainpage/delivery.png"
                      className="responsive"
                      alt="intro"
                    />
                  </div>

                  <div className="hoverImg">
                    <img
                      rel="preload"
                      srcSet="/assets/mainpage/deliveryboomer.png, /assets/mainpage/deliveryboomer@2x.png 2x"
                      src="/assets/mainpage/deliveryboomer.png"
                      className="boomer"
                      alt="intro"
                    />
                  </div>
                </div>
                <div className="menuBox">
                  <div className="chef-image">
                    <img
                      rel="preload"
                      srcSet="/assets/mainpage/waitress.png, /assets/mainpage/waitress@2x.png 2x"
                      src="/assets/mainpage/waitress.png"
                      className="responsive"
                      alt="intro"
                    />
                  </div>
                  <div className="hoverImg">
                    <img
                      rel="preload"
                      srcSet="/assets/mainpage/waiterboomer.png, /assets/mainpage/waiterboomer@2x.png 2x"
                      src="/assets/mainpage/waiterboomer.png"
                      className="biggerboomer"
                      alt="intro"
                    />
                  </div>
                </div>
                <div className="cashierBox">
                  <div className="cashier-image">
                    <img
                      rel="preload"
                      srcSet="/assets/mainpage/cashier.png, /assets/mainpage/cashier@2x.png 2x"
                      src="/assets/mainpage/cashier.png"
                      className="responsive"
                      alt="intro"
                    />
                  </div>
                  <div className="hoverImg">
                    <img
                      rel="preload"
                      srcSet="/assets/mainpage/cashierboomer.png, /assets/mainpage/cashierboomer@2x.png 2x"
                      src="/assets/mainpage/cashierboomer.png"
                      className="mediumboomer"
                      alt="intro"
                    />
                  </div>
                </div>
                <div className="intro-image">
                  <img
                    srcSet="/assets/mainpage/mainphone.png, /assets/mainpage/mainphone@2x.png 2x"
                    src="/assets/mainpage/mainphone.png"
                    className="responsive"
                    alt="intro"
                  />
                </div>
                <div className="intro-content">
                  <img
                    srcSet="/assets/mainpage/maincontent.png, /assets/mainpage/maincontent@2x.png 2x"
                    src="/assets/mainpage/maincontent.png"
                    className="responsive"
                    alt="intro"
                  />
                </div>

                <div className="hoverairtext">
                  <img
                    rel="preload"
                    srcSet="/assets/mainpage/flightattendanttext.png, /assets/mainpage/flightattendanttext@2x.png 2x"
                    src="/assets/mainpage/flightattendanttext.png"
                    className="responsive"
                    alt="intro"
                  />
                </div>
                <div className="hoverhardwaretext">
                  <img
                    rel="preload"
                    srcSet="/assets/mainpage/hardwaretext.png, /assets/mainpage/hardwaretext@2x.png 2x"
                    src="/assets/mainpage/hardwaretext.png"
                    className="responsive"
                    alt="intro"
                  />
                </div>
                <div className="hoverbaristatext">
                  <img
                    rel="preload"
                    srcSet="/assets/mainpage/baristatext.png, /assets/mainpage/baristatext@2x.png 2x"
                    src="/assets/mainpage/baristatext.png"
                    className="responsive"
                    alt="intro"
                  />
                </div>
                <div className="hoverdeliverytext">
                  <img
                    rel="preload"
                    srcSet="/assets/mainpage/deliveryguytext.png, /assets/mainpage/deliveryguytext@2x.png 2x"
                    src="/assets/mainpage/deliveryguytext.png"
                    className="responsive"
                    alt="intro"
                  />
                </div>
                <div className="hovercashiertext">
                  <img
                    rel="preload"
                    srcSet="/assets/mainpage/cashiertext.png, /assets/mainpage/cashiertext@2x.png 2x"
                    src="/assets/mainpage/cashiertext.png"
                    className="responsive"
                    alt="intro"
                  />
                </div>
                <div className="hovercheftext">
                  <img
                    rel="preload"
                    srcSet="/assets/mainpage/waitresstext.png, /assets/mainpage/waitresstext@2x.png 2x"
                    src="/assets/mainpage/waitresstext.png"
                    className="responsive"
                    alt="intro"
                  />
                </div>
              </div>
              {this.state.termsAccepted && (
                <img
                  srcSet="/assets/mainpage/thanksboomer.png, /assets/mainpage/thanksboomer@2x.png 2x"
                  src="/assets/mainpage/thanksboomer.png"
                  alt="thankyou"
                  className="thanksboomer"
                />
              )}
              <div className="responsiveFlex">
                <Link
                  activeClass="active"
                  to="scrollHere"
                  spy={true}
                  smooth={true}
                  // offset={50}
                  duration={500}
                  onSetActive={this.handleSetActive}
                >
                  <img
                    srcSet="/assets/mainpage/sketch-down.png, /assets/mainpage/sketch-down@2x.png 2x"
                    src="/assets/mainpage/sketch-down.png"
                    alt="down"
                    className="introdown"
                  />
                </Link>
                <Element className="scrollHere" name="scrollHere">
                  <p className="introtext">
                    HiThrive is changing the way people appreciate one another.
                    Join us if you’re interested in having a meaningful impact
                    on the future of positive interactions.
                  </p>
                </Element>
                <Input
                  // autoFocus
                  onChange={this.handleChange}
                  type="email"
                  className="email-box"
                  name="email"
                  id="email"
                />
                <Button
                  onClick={this.handleOpen}
                  type="button"
                  className="email-button"
                >
                  email me when it's ready
                </Button>
                <div
                  className={
                    "signupmodal " + (this.state.modalOpen ? "show-modal" : "")
                  }
                >
                  <div className="modal-content">
                    <img
                      srcSet="/assets/mainpage/emailbubble.png, /assets/mainpage/emailbubble@2x.png 2x"
                      src="/assets/mainpage/emailbubble.png"
                      className={
                        "emailbubble " +
                        (this.state.modalOpen ? "show-modal" : "")
                      }
                    />
                  </div>
                  <div className="modal-content">
                    <div className="nobordermodal-header">
                      <div className="flexcenterrow">
                        <h3 className="header-text">Sign up with </h3>
                        {/* <h3 className="header-text-option">Facebook</h3> */}
                        <FacebookLogin
                          appId="688108541538759"
                          fields="name,email,picture.width(220).height(235)"
                          callback={response => {
                            this.responseFacebook(response);
                          }}
                          render={renderProps => (
                            <h3
                              className="header-text-option"
                              onClick={renderProps.onClick}
                            >
                              Facebook
                            </h3>
                          )}
                        />
                        <h3 className="header-text">, </h3>
                        {/* <h3 className="header-text-option">Twitter</h3> */}
                        {/* <div> */}
                        <TwitterLogin
                          loginUrl={
                            process.env.REACT_APP_HEROKU_STAGING +
                            "/api/user/twitter/auth"
                          }
                          onFailure={this.onFailed}
                          onSuccess={this.onTwtterSuccess}
                          requestTokenUrl={
                            process.env.REACT_APP_HEROKU_STAGING +
                            "/api/user/twitter/reverse"
                          }
                        >
                          {/* Twitter */}
                          <h3 className="header-text-option">Twitter</h3>
                        </TwitterLogin>
                        {/* </div> */}
                        <h3 className="header-text"> , or </h3>
                        <GoogleLogin
                          clientId="48507921891-tn1j0jhigq5dretfvavj00ulvd9rafqf.apps.googleusercontent.com"
                          onSuccess={response => this.responseGoogle(response)}
                          onFailure={fail => this.failGoogle(fail)}
                          render={renderProps => (
                            <h3
                              className="header-text-option"
                              onClick={renderProps.onClick}
                            >
                              Google
                            </h3>
                          )}
                        />
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="flexrow">
                      <img
                        srcSet="/assets/mainpage/orline.png, /assets/mainpage/orline@2x.png 2x"
                        src="/assets/mainpage/orline.png"
                        className="orline"
                      />
                      <h4 className="sidemargin">or</h4>
                      <img
                        srcSet="/assets/mainpage/orline.png, /assets/mainpage/orline@2x.png 2x"
                        src="/assets/mainpage/orline.png"
                        className="orline"
                      />
                    </div>
                    <Input
                      onChange={this.handleChange}
                      fluid
                      required={true}
                      id="firstname"
                      icon="user outline"
                      placeholder="First name"
                      value={this.state.firstname}
                      error={
                        !this.state.firstname && this.state.triedsubmit
                          ? true
                          : false
                      }
                    />
                    <Input
                      onChange={this.handleChange}
                      icon="user outline"
                      fluid
                      value={this.state.lastname}
                      id="lastname"
                      placeholder="Last name"
                    />
                    <Input
                      onChange={this.handleChange}
                      placeholder="joe@schmoe.com"
                      icon="envelope outline"
                      type="email"
                      id="email"
                      value={this.state.email}
                      error={
                        !this.state.email && this.state.triedsubmit
                          ? true
                          : false
                      }
                    />
                    <Input
                      onChange={this.handleChange}
                      id="phone"
                      icon="phone"
                      placeholder="10 - digit phone #"
                      error={
                        !this.state.phone && this.state.triedsubmit
                          ? true
                          : false
                      }
                    />
                    <div className="left">
                      <Checkbox
                        className="fontbox"
                        onChange={this.toggle}
                        checked={this.state.customerfacing}
                        id="customerfacing"
                        label="I WORK IN A CUSTOMER FACING ROLE"
                      />
                      <Popup
                        trigger={
                          <img
                            srcSet="/assets/mainpage/infoicon.png, /assets/mainpage/infoicon@2x.png 2x"
                            src="/assets/mainpage/infoicon.png"
                            onClick={this.infoicon}
                            className="infoicon"
                          />
                        }
                        content="Checking this box will prompt you to enter your employment information and set up your account as an associate. This gives other users the ability to share appreciation with you."
                      />
                    </div>
                    <Button
                      onClick={this.submitSignUp}
                      type="button"
                      className="signup-button"
                    >
                      Sign Up
                    </Button>
                  </div>
                </div>
                <div
                  className={
                    "userExistsModal " +
                    (this.state.userExists ? "show-userexistsmodal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          We already have your number!
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">
                        Looks like you’ve already joined! Please contact us at
                        support@hithrive.com if we’ve made a mistake.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "verificationmodal " +
                    (this.state.verificationOpen
                      ? "show-verificationmodal"
                      : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-header">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">WE TEXTED YOU</h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="flexheightcentertext">
                      <p className="semimodaltext">
                        PLEASE VERIFY THE 4 - DIGIT NUMBER THAT WE SENT YOU.
                      </p>

                      <div className="verifycoderow">
                        <Input
                          id="verifycode1"
                          autoFocus
                          onChange={this.handleCodeChange}
                          error={this.state.triedsubmit ? true : false}
                          className="verifycode1"
                          ref={c => (this.inputref1 = c)}
                          maxLength="1"
                        />
                        <Input
                          id="verifycode2"
                          onChange={this.handleCodeChange2}
                          error={this.state.triedsubmit ? true : false}
                          className="verifycode2"
                          ref={c => (this.inputref2 = c)}
                          maxLength="1"
                        />
                        <Input
                          id="verifycode3"
                          onChange={this.handleCodeChange3.bind(this)}
                          error={this.state.triedsubmit ? true : false}
                          ref={this.inputref3}
                          className="verifycode3"
                          maxLength="1"
                        />
                        <Input
                          id="verifycode4"
                          onChange={this.handleCodeChange4.bind(this)}
                          error={this.state.triedsubmit ? true : false}
                          className="verifycode4"
                          ref={this.inputref4}
                          maxLength="1"
                        />
                      </div>
                      <a
                        onClick={this.getanotherVerification}
                        className="leftaction-text-option"
                      >
                        I DIDN'T RECEIVE THE CODE
                      </a>
                      <div className="verification-actions">
                        <Button
                          onClick={this.submitVerification}
                          type="button"
                          className="signup-button"
                        >
                          Confirm
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "employmentsocialmodal " +
                    (this.state.employmentSocialOpen
                      ? "show-employmentsocialmodal"
                      : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-header">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">EMPLOYMENT INFO</h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <Input
                      fluid
                      id="role"
                      onChange={this.handleChange}
                      placeholder="Role"
                      icon="user"
                      className="nomargin"
                    />

                    <ReactGoogleMapLoader
                      params={{
                        key: process.env.REACT_APP_GOOGLE_PLACES,
                        libraries: "places,geocode"
                      }}
                      render={googleMaps =>
                        googleMaps && (
                          <div>
                            <ReactGooglePlacesSuggest
                              autocompletionRequest={{
                                input: this.state.employersearch
                              }}
                              googleMaps={googleMaps}
                              textNoResults="New Employer" // null or "" if you want to disable the no results item
                              onSelectSuggest={this.handleEmployerSelectSuggest}
                            >
                              <input
                                type="text"
                                value={this.state.employervalue}
                                placeholder="Employer"
                                onChange={this.handleEmployerChange}
                              />
                            </ReactGooglePlacesSuggest>
                          </div>
                        )
                      }
                    />

                    <ReactGoogleMapLoader
                      params={{
                        key: process.env.REACT_APP_GOOGLE_PLACES,
                        libraries: "places,geocode"
                      }}
                      render={googleMaps =>
                        googleMaps && (
                          <div>
                            <ReactGooglePlacesSuggest
                              autocompletionRequest={{
                                input: this.state.locationsearch
                              }}
                              googleMaps={googleMaps}
                              textNoResults="New Location" // null or "" if you want to disable the no results item
                              onSelectSuggest={this.handleLocationSelectSuggest}
                            >
                              <input
                                type="text"
                                value={this.state.locationvalue}
                                placeholder="Search a location"
                                onChange={this.handleLocationChange}
                              />
                            </ReactGooglePlacesSuggest>
                          </div>
                        )
                      }
                    />

                    <Input
                      fluid
                      id="employeeid"
                      onChange={this.handleChange}
                      placeholder="Employee ID (if applicable)"
                      className="nomargin"
                    />
                    <p className="lefttext" onClick={this.openProfile}>
                      sign up as a customer
                    </p>
                    <div className="verification-actions">
                      <Button
                        onClick={this.submitEmployment}
                        type="button"
                        className="signup-button"
                      >
                        Confirm
                      </Button>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "outofMarketModal " +
                    (this.state.outofMarket ? "show-outofMarketModal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          We’re not in your area Yet :(
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">
                        For our first launch, we are focusing on the NYC-area
                        only. However, we have saved your information and will
                        notify you as soon as we are available in your area! You
                        may continue to use the app in the meantime. However,
                        people will not be able to search for you yet.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "employmentmodal " +
                    (this.state.employmentOpen ? "show-employmentmodal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-header">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">EMPLOYMENT INFO</h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <Input
                      fluid
                      id="role"
                      icon="user"
                      onChange={this.handleChange}
                      placeholder="Role"
                      className="nomargin"
                    />

                    <Form className="flexform">
                      <ReactGoogleMapLoader
                        fluid
                        params={{
                          key: process.env.REACT_APP_GOOGLE_PLACES,
                          libraries: "places,geocode"
                        }}
                        icon="user"
                        render={googleMaps =>
                          googleMaps && (
                            <div>
                              <ReactGooglePlacesSuggest
                                autocompletionRequest={{
                                  input: this.state.employersearch
                                }}
                                googleMaps={googleMaps}
                                textNoResults="New Employer" // null or "" if you want to disable the no results item
                                onSelectSuggest={
                                  this.handleEmployerSelectSuggest
                                }
                              >
                                <input
                                  type="text"
                                  value={this.state.employervalue}
                                  placeholder="Employer"
                                  onChange={this.handleEmployerChange}
                                />
                              </ReactGooglePlacesSuggest>
                            </div>
                          )
                        }
                      />
                      <ReactGoogleMapLoader
                        params={{
                          key: process.env.REACT_APP_GOOGLE_PLACES,
                          libraries: "places,geocode"
                        }}
                        className="ui fluid input nomargin"
                        render={googleMaps =>
                          googleMaps && (
                            <div>
                              <ReactGooglePlacesSuggest
                                autocompletionRequest={{
                                  input: this.state.locationsearch
                                }}
                                googleMaps={googleMaps}
                                textNoResults="New Location" // null or "" if you want to disable the no results item
                                onSelectSuggest={
                                  this.handleLocationSelectSuggest
                                }
                              >
                                <input
                                  type="text"
                                  value={this.state.locationvalue}
                                  placeholder="Search a location"
                                  onChange={this.handleLocationChange}
                                />
                              </ReactGooglePlacesSuggest>
                            </div>
                          )
                        }
                      />
                    </Form>
                    <Input
                      fluid
                      id="employeeid"
                      onChange={this.handleChange}
                      placeholder="Employee ID (if applicable)"
                      className="nomargin"
                    />
                    <div className="verification-actions">
                      <Button
                        onClick={this.submitEmployment}
                        type="button"
                        className="signup-button"
                      >
                        Confirm
                      </Button>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "uploadmodal " +
                    (this.state.uploadModalOpen ? "show-uploadmodal" : "")
                  }
                >
                  <div className="modal-content">
                    <img
                      srcSet="/assets/mainpage/photoboomerbubble.png, /assets/mainpage/photoboomerbubble@2x.png 2x"
                      src="/assets/mainpage/photoboomerbubble.png"
                      className={
                        "photoboomerbubble " +
                        (this.state.modalOpen ? "show-modal" : "")
                      }
                    />
                  </div>
                  <div className="modal-content">
                    <div className="modal-header">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">UPLOAD PHOTO</h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="flexcentercolumntext">
                      <div>
                        <p className="semimodaltextcenter">
                          UPLOAD A PHOTO OF YOURSELF
                        </p>
                        <div className="fitbox">
                          <Avatar
                            width={220}
                            height={235}
                            onCrop={this.onCrop}
                            onClose={this.onClose}
                            src={this.state.src}
                            label="ADD PHOTO"
                            borderStyle={{
                              border: "4px solid #64CFDD",
                              textAlign: "center",
                              borderRadius: "50%",
                              mozBorderRadius: "50%",
                              webkitBorderRadius: "50%"
                            }}
                            labelStyle={{
                              color: "rgba(51, 51, 51, 0.7) !important",
                              fontSize: "14px !important",
                              width: "220px !important",
                              letterSpacing: "3px !important",
                              fontFamily: "Proxima Nova Semibold !important"
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="verification-actions">
                      <Button
                        onClick={this.submitPhotoUpload}
                        type="button"
                        className="signup-button"
                      >
                        Confirm
                      </Button>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "termsmodal " +
                    (this.state.termsOpen ? "show-termsmodal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          BEFORE YOU BEGIN
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="flexleftcenter">
                      <p className="termsText">
                        Please agree to be respectful of other users (both
                        associates and customers).
                      </p>
                      <p className="termsText">
                        HiThrive is a platform built on the basis of showing
                        appreciation to customer-facing associates. If you have
                        complaints about an experience, this is not the place to
                        voice them.
                      </p>
                      <p className="termsText">
                        Joining HiThrive gives you the opportunity to help
                        associates receive the uplifting recognition that they
                        deserve. By agreeing to these terms, you are agreeing to
                        be a part of this community.
                      </p>
                    </div>

                    <div className="verification-actions">
                      <Button
                        onClick={this.submitTerms}
                        type="button"
                        className="signup-button"
                      >
                        I AGREE
                      </Button>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "privacyModal " +
                    (this.state.privacyModal ? "show-privacyModal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          HiThrive Privacy Policy
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">
                        Effective: August 10, 2018 This policy explains what
                        information we collect when you use HiThrive’s sites,
                        services, mobile applications, products, and content
                        (“Services”). It also has information about how we
                        store, use, transfer, and delete that information.
                        Information We Collect & How We Use It In order to give
                        you the best possible experience using HiThrive, we
                        collect information from your interactions with our
                        network. Some of this information, you actively tell us
                        (such as your email address and phone number, which we
                        use to track your account or communicate with you).
                        Other information, we collect based on actions you take
                        while using HiThrive, such as what content you access
                        and your interactions with our product features (like
                        comments, search, follows, and HiFives). This
                        information includes records of those interactions, your
                        Internet Protocol address, information about your device
                        (such as device or browser type), and referral
                        information. We use cookies and similar technologies
                        (like web beacons, pixels, ad tags and device
                        identifiers) to recognize you and/or your device(s) on,
                        off and across different Services and devices. You can
                        control cookies through your browser settings and other
                        tools. We use this information to: provide, test,
                        improve, promote and personalize HiThrive Services
                        generate relevant content and information including
                        advertising fight spam and other forms of abuse generate
                        aggregate, non-identifying information about how people
                        use HiThrive Services When you create your HiThrive
                        account, and authenticate with a third-party service
                        (like Twitter, Facebook or Google) we may collect,
                        store, and periodically update information associated
                        with that third-party account, such as your lists of
                        friends or followers. We will never publish through your
                        third-party account without your permission. Information
                        Disclosure Any data that you include on your profile and
                        any content you post or social action (like comments,
                        search, follows, and HiFives) you take on our Services
                        will be seen by others. We may share your account
                        information with third parties in some circumstances,
                        including: (1) with your consent; (2) to a service
                        provider or partner who meets our data protection
                        standards; (3) when we have a good faith belief it is
                        required by law, such as pursuant to a subpoena or other
                        legal process; (4) when we have a good faith belief that
                        doing so will help prevent imminent harm to someone. If
                        we are going to share your information in response to
                        legal process, we’ll give you notice so you can
                        challenge it (for example by seeking court
                        intervention), unless we’re prohibited by law or believe
                        doing so may endanger others. We will object to requests
                        for information about users of our services that we
                        believe are improper. Data Storage HiThrive uses
                        third-party vendors and hosting partners, such as
                        Amazon, for hardware, software, networking, storage, and
                        related technology we need to run HiThrive. We maintain
                        two types of logs: server logs and event logs. By using
                        HiThrive Services, you authorize HiThrive to transfer,
                        store, and use your information in the United States and
                        any other country where we operate. Modifying or
                        Deleting Your Personal Information If you have a
                        HiThrive account, you can access or modify your personal
                        information, or delete your account. HiThrive retains
                        personal data associated with your account for the
                        lifetime of your account. If you would like to delete
                        your personal information, you can delete your account
                        at any time. Upon deletion, your personal data will
                        generally stop being visible to others on our Services
                        within 24 hours. We generally delete closed account
                        information within 30 days of account closure, except as
                        noted below. We retain your personal data even after you
                        have closed your account when required to do so by law.
                        We will retain de-personalized information after your
                        account has been closed. Information you have shared
                        with others will remain visible after you closed your
                        account or deleted the information from your own profile
                        or inbox. Shared content associated with closed accounts
                        will show an unknown user as the source. Your profile
                        may continue to be displayed in the services of others
                        (e.g., search engine results) until they refresh their
                        cache. Data Security We use encryption (HTTPS/TLS) to
                        protect data transmitted to and from our site. However,
                        no data transmission over the Internet is 100% secure,
                        so we can’t guarantee security. You use the Service at
                        your own risk, and you’re responsible for taking
                        reasonable measures to secure your account. Business
                        Transfers If we are involved in a merger, acquisition,
                        bankruptcy, reorganization or sale of assets such that
                        your information would be transferred or become subject
                        to a different privacy policy, we’ll notify you in
                        advance so you can opt out of any such new policy by
                        deleting your account before transfer. Email from
                        HiThrive Sometimes we’ll send you emails about your
                        account, service changes or new policies. You can’t opt
                        out of this type of “transactional” email (unless you
                        delete your account). But, you can opt out of
                        non-administrative emails such as digests, newsletters,
                        and activity notifications through your account’s
                        “Settings” page. Changes to this Policy HiThrive may
                        periodically update this Policy. We’ll notify you about
                        significant changes to it. The most current version of
                        the policy will always be available at HiThrive.com and
                        under “Settings” within the mobile application.
                        Questions We welcome feedback about this policy at
                        legal@hithrive.com. Contact Information HiThrive
                        Incorporated 447 Broadway, 2nd Floor New York, NY 10013
                        United States of America
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "productTermsModal " +
                    (this.state.productTermsModal
                      ? "show-productTermsModal"
                      : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          HiThrive Terms of Service
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">
                        Effective: August 10, 2018 These Terms of Service
                        (“Terms”) are a contract between you and HIThrive
                        Incorporated (“HiThrive”). They govern your use of
                        HiThrive’s sites, services, mobile apps, products, and
                        content (“Services”). By using HiThrive, you agree to
                        these Terms. Your use of our Services is also subject to
                        our our Privacy Policy, which covers how we collect,
                        use, share, and store your personal information. If you
                        don’t agree to any of the Terms, you can’t use HiThrive.
                        We can change these Terms at any time. If a change is
                        material, we’ll let you know before they take effect. By
                        using HiThrive on or after that effective date, you
                        agree to the new Terms. If you don’t agree to them, you
                        should delete your account before they take effect,
                        otherwise your use of the Services will be subject to
                        the new Terms. Rights & Limits You own all of the
                        content and personal information you provide to us, but
                        by using HiThrive you also grant us a non-exclusive
                        license to it. The non-exclusive license is a worldwide,
                        transferable and sublicensable right to use, copy,
                        modify, distribute, publish, and process, information
                        and content that you provide through our Services,
                        without any further consent, notice and/or compensation
                        to you or others. You can end this license for specific
                        content by deleting such content from the Services, or
                        generally by closing your account, except (a) to the
                        extent you shared it with others as part of the Service
                        and they copied, re-shared it or stored it and (b) for
                        the reasonable time it takes to remove from backup and
                        other systems. We will not include your content in
                        advertisements for the products and services of third
                        parties to others without your separate consent
                        (including sponsored content). However, we have the
                        right, without payment to you or others, to serve ads
                        near your content and information, and your social
                        actions may be visible and included with ads, as noted
                        in the Privacy Policy. We will get your consent if we
                        want to give others the right to publish your content
                        beyond the Services. However, if you choose to share
                        your post as "public", we will enable a feature that
                        allows other Members to embed that public post onto
                        third-party services, and we enable search engines to
                        make that public content findable though their services.
                        While we may edit and make format changes to your
                        content (such as translating it, modifying the size,
                        layout or file type or removing metadata), we will not
                        modify the meaning of your expression. You and HiThrive
                        agree that if content includes personal data, it is
                        subject to our Privacy Policy. You and HiThrive agree
                        that we may access, store, process and use any
                        information and personal data that you provide in
                        accordance with the terms of the Privacy Policy and your
                        choices (including settings). By submitting suggestions
                        or other feedback regarding our Services to HiThrive,
                        you agree that HiThrive can use and share (but does not
                        have to) such feedback for any purpose without
                        compensation to you. You agree to only provide content
                        or information that does not violate the law nor
                        anyone’s rights (including intellectual property
                        rights). You also agree that your profile information
                        will be truthful. HiThrive may be required by law to
                        remove certain information or content in certain
                        countries. Our content and services We reserve all
                        rights in HiThrive’s look and feel. You may not copy or
                        adapt any portion of our code or visual design elements
                        (including logos) without express written permission
                        from HiThrive unless otherwise permitted by law. You may
                        not do, or try to do, the following: (1) access or
                        tamper with non-public areas of the Services, our
                        computer systems, or the systems of our technical
                        providers; (2) access or search the Services by any
                        means other than the currently available, published
                        interfaces (e.g., APIs) that we provide; (3) forge any
                        TCP/IP packet header or any part of the header
                        information in any email or posting, or in any way use
                        the Services to send altered, deceptive, or false
                        source-identifying information; or (4) interfere with,
                        or disrupt, the access of any user, host, or network,
                        including sending a virus, overloading, flooding,
                        spamming, mail-bombing the Services, or by scripting the
                        creation of content or accounts in such a manner as to
                        interfere with or create an undue burden on the
                        Services. Crawling the Services is allowed, but scraping
                        the Services is prohibited. We may change, terminate, or
                        restrict access to any aspect of the service, at any
                        time, without notice. No children HiThrive is only for
                        people 13 years old and over. By using HiThrive, you
                        affirm that you are over 13. If we learn someone under
                        13 is using HiThrive, we’ll terminate their account.
                        Security If you find a security vulnerability on
                        HiThrive, tell us. We are developing a bug bounty
                        disclosure program and will compensate you.
                        Vulnerabilities can be reported to
                        security@hithrive.com. Incorporated rules and policies
                        By using the Services, you agree to let HiThrive collect
                        and use information as detailed in our Privacy Policy.
                        If you’re outside the United States, you consent to
                        letting HiThrive transfer, store, and process your
                        information (including your personal information and
                        content) in and out of the United States. To enable a
                        functioning community, we have Rules and by using
                        HiThrive you agree to follow these Rules. If you don’t,
                        we may remove content, or suspend or delete your
                        account. Miscellaneous Disclaimer of warranty. HiThrive
                        provides the Services to you as is. You use them at your
                        own risk and discretion. That means they don’t come with
                        any warranty. None express, none implied. No implied
                        warranty of merchantability, fitness for a particular
                        purpose, availability, security, title or
                        non-infringement. Limitation of Liability. HiThrive
                        won’t be liable to you for any damages that arise from
                        your using the Services. This includes if the Services
                        are hacked or unavailable. This includes all types of
                        damages (indirect, incidental, consequential, special or
                        exemplary). And it includes all kinds of legal claims,
                        such as breach of contract, breach of warranty, tort, or
                        any other loss. No waiver. If HiThrive doesn’t exercise
                        a particular right under these Terms, that doesn’t waive
                        it. Severability. If any provision of these terms is
                        found invalid by a court of competent jurisdiction, you
                        agree that the court should try to give effect to the
                        parties’ intentions as reflected in the provision and
                        that other provisions of the Terms will remain in full
                        effect. Choice of law and jurisdiction. These Terms are
                        governed by New York law, without reference to its
                        conflict of laws provisions. You agree that any suit
                        arising from the Services must take place in a court
                        located in New York, New York. Entire agreement. These
                        Terms (including any document incorporated by reference
                        into them) are the whole agreement between HiThrive and
                        you concerning the Services. Questions? Let us know at
                        legal@hithrive.com.
                      </p>
                    </div>
                  </div>
                </div>
                <div
                  className={
                    "contactModal " +
                    (this.state.contactModal ? "show-contactModal" : "")
                  }
                >
                  <div className="modal-content">
                    <div className="nobordermodal-paddedheader">
                      <div className="flexcenterrow">
                        <h3 className="modalheadertextbold">
                          We aren't there yet!
                        </h3>
                      </div>

                      <div className="exit">
                        <img
                          srcSet="/assets/mainpage/exit.png, /assets/mainpage/exit@2x.png 2x"
                          src="/assets/mainpage/exit.png"
                          onClick={this.exitModals}
                          className="exit"
                        />
                      </div>
                    </div>
                    <div className="modaltextcontainer">
                      <p className="termsText">but we will be!</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Row className="section-2">
            <Col lg={6} md={6} sm={12} xs={12} className="section-text-a-1">
              <p className="header-number-a">1.</p>
              <p className="header-title">Find Someone</p>
              <img
                srcSet="/assets/mainpage/line.png, /assets/mainpage/line@2x.png 2x"
                src="/assets/mainpage/line.png"
                sizes="(max-width: 320px) 280px,
                                            (max-width: 480px) 440px,
                                            800px"
                className="line"
                alt="line"
              />
              <p className="body-right">
                The only social platform where you can attribute positive
                feedback directly to an employee.
              </p>
            </Col>
            <div className="phone2col">
              <picture>
                <img
                  srcSet="/assets/mainpage/phone2.png, /assets/mainpage/phone2@2x.png 2x"
                  src="/assets/mainpage/phone2.png"
                  className="phone2"
                  alt="search"
                />
              </picture>
            </div>
          </Row>
          <Row className="section-3">
            <div className="phone3col">
              <img
                srcSet="/assets/mainpage/phone3.png, /assets/mainpage/phone3@2x.png 2x"
                src="/assets/mainpage/phone3.png"
                className="phone3"
                alt="positive"
              />
            </div>
            <Col className="section-text-b" lg={6} md={6} sm={12} xs={12}>
              <p className="header-number-b">2.</p>
              <p className="header-title-b">
                Say Something Positive & Share it
              </p>
              <img
                srcSet="/assets/mainpage/line.png, /assets/mainpage/line@2x.png 2x"
                src="/assets/mainpage/line.png"
                sizes="(max-width: 320px) 280px,
                                            (max-width: 480px) 440px,
                                            800px"
                className="line"
                alt="line"
              />
              <p className="body-left">
                A positive only platform, HiThrive subscribes to the adage “If
                you don’t have something nice to say, don’t say anything at
                all.”
              </p>
            </Col>
          </Row>
          <Row className="section-4">
            <Col lg={6} md={6} sm={12} xs={12} className="section-text-a-2">
              <p className="header-number-a">3.</p>
              <p className="header-title">Thrive!</p>
              <img
                sizes="(max-width: 320px) 280px,
                                            (max-width: 480px) 440px,
                                            800px"
                srcSet="/assets/mainpage/line3.png, /assets/mainpage/line3@2x.png 2x"
                src="/assets/mainpage/line3.png"
                className="line"
                alt="line"
              />
              <p className="body-right-b">
                Collect gratitude to fuel your career. Whether you’re achieving
                milestones on the platform or being rewarded in your place of
                work, your employer will never again wonder what your value-add
                is. Allow us to cc your manager on each HiTHRIVE received and
                the process will work while you do.
              </p>
            </Col>
            <div className="phone4col">
              <img
                srcSet="/assets/mainpage/phone4.png, /assets/mainpage/phone4@2x.png 2x"
                src="/assets/mainpage/phone4.png"
                className="phone4"
                alt="Thrive"
              />
            </div>
          </Row>
          <Row className="section-footer">
            <div className="flex-row-footer">
              <h3 className="footer-header-home">
                <a href="#" className="footer-header-home">
                  HiThrive, 2018
                </a>
              </h3>
              <h3 className="footer-header-option" onClick={this.PrivacyPolicy}>
                Privacy Policy
              </h3>
              <h3 className="footer-header-option" onClick={this.productTerm}>
                Terms
              </h3>
              <h3 className="footer-header-option" onClick={this.contactModal}>
                Contact
              </h3>
            </div>
            <div className="flexrowlinks">
              <li className="hiddenbullet">
                <a href="https://facebook.com/Hithrive">
                  <img
                    srcSet={this.state.fbsrcset}
                    src={this.state.fbsrc}
                    onClick={this.goto("https://facebook.com/Hithrive")}
                    className="footer-header-fb"
                    onMouseEnter={this.fblinkhover}
                    onMouseLeave={this.fblink}
                  />
                </a>
              </li>
              <li className="hiddenbullet">
                <a href="https://instagram.com/hithrive">
                  <img
                    srcSet={this.state.igsrcset}
                    src={this.state.igsrc}
                    onClick={this.goto("https://instagram.com/hithrive")}
                    className="footer-header-ig"
                    onMouseEnter={this.iglinkhover}
                    onMouseLeave={this.iglink}
                  />
                </a>
              </li>
              <li className="hiddenbullet">
                <a href="https://twitter.com/hithrive">
                  <img
                    srcSet={this.state.twittersrcset}
                    src={this.state.twittersrc}
                    onClick={this.goto("https://twitter.com/hithrive")}
                    className="footer-header-twitter"
                    onMouseEnter={this.twitterlinkhover}
                    onMouseLeave={this.twitterlink}
                  />
                </a>
              </li>
            </div>
          </Row>
        </Container>
      );
    }
  }
}

export default App;
